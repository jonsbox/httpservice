import aiohttp
import json
from tornado.web import RequestHandler, MissingArgumentError


class ProtoHandler(RequestHandler):
    _cerror = None

    def ok(self, data):
        self.write({
            'result': True,
            'data': data,
            'error': None
        })
        return

    def error(self, error):
        self.write({
            'result': False,
            'data': None,
            'error': error
        })

    def _core_down(self):
        self.error('Сервис недоступен')

    def _core_error(self):
        self.error(self._cerror)

    @property
    def data(self):
        return json.loads(self.request.body) if self.request.method == 'POST' else None


class BaseHandler(ProtoHandler):
    def initialize(self) -> None:
        def json_error():
            return self.error('Не json')

        if self.request.method == 'POST':
            try:
                self.data
            except ValueError:
                self.post = json_error

    async def prepare(self) -> None:
        async with aiohttp.ClientSession() as session:
            try:
                res = await session.post(
                    'http://localhost:4540/check_user',
                    data=json.dumps({
                        'session': self.get_cookie('session')
                    })
                )
            except aiohttp.client.ClientConnectionError:
                self.get = self.post = self._core_down
                return
            if res.status != 200:
                self.get = self.post = self._core_down
                return

            res = json.loads(await res.content.read())
            if res['result']:
                self.current_user = res['data']
            else:
                self._cerror = res['error']
                self.get = self.post = self._core_error

    async def get_core_data(self, url, method, data=None):
        if 'device_token' in self.cookies:
            update_token, token = True, (self.get_cookie('device_token') or None)
        else:
            update_token = token = None
        if method == 'post':
            if data:
                data['user'] = self.current_user
            else:
                data = {'user': self.current_user}
            if update_token:
                data['user']['device_token'] = token
            data = json.dumps(data)
        elif method == 'get':
            if data:
                data['user'] = self.current_user['id']
            else:
                data = {'user': self.current_user['id']}
        else:
            raise ValueError(f'Неизвестный метод {method}')

        try:
            async with aiohttp.ClientSession() as session:
                res = await getattr(session, method)(
                    f'http://localhost:4540/{url}',
                    data=data
                )
                if res.status != 200:
                    self._core_down()
                    return

                res = json.loads(await res.content.read())
                if res['result']:
                    self.ok(res['data'])
                else:
                    self.error(res['error'])
        except aiohttp.client.ClientConnectionError:
            self.get = self.post = self._core_down


class RegisterHandler(ProtoHandler):
    async def post(self):
        async with aiohttp.ClientSession() as session:
            res = await session.post(
                'http://localhost:4540/register',
                data=self.request.body
            )
            if res.status != 200:
                self.get = self.post = self._core_down
                return

            res = json.loads(await res.content.read())
            if res['result']:
                self.ok(res['data'])
            else:
                self.error(res['error'])


class LoginHandler(ProtoHandler):
    async def post(self):
        async with aiohttp.ClientSession() as session:
            res = await session.post(
                'http://localhost:4540/login',
                data=self.request.body
            )
            if res.status != 200:
                self.get = self.post = self._core_down
                return

            res = json.loads(await res.content.read())
            if res['result']:
                session = res['data']['session']
                self.set_cookie('session', session)
                self.ok(session)
            else:
                self.error(res['error'])


class StartAppHandler(BaseHandler):
    async def get(self):
        await self.get_core_data('start_app', 'post')


class NewGameHandler(BaseHandler):
    async def get(self):
        await self.get_core_data('new_game', 'get', {
            'contest': self.get_argument('contest', '0')
        })

    async def post(self):
        data = self.data
        try:
            theme = data['theme']
            contest = data.get('contest', False)
            overrecord = data.get('overrecord', False)
        except KeyError:
            self.error('Не указана тема')
            return

        core_data = {
            'theme': theme,
            'contest': contest,
            'overrecord': overrecord
        }
        await self.get_core_data('new_game', 'post', core_data)


class GetQuestionHandler(BaseHandler):
    async def get(self):
        try:
            game_id = int(self.get_argument('game'))
            await self.get_core_data('get_question', 'get', {'game': game_id})
        except (MissingArgumentError, ValueError):
            self.error(u'Не указана игра')


class AnswerHandler(BaseHandler):
    async def post(self):
        data = self.data
        try:
            pars = {
                'game': data['game'],
                'answer': data['answer'],
                'timer_left': data['timer_left']
            }
            await self.get_core_data('answer', 'post', pars)
        except KeyError as e:
            self.error(f'Не указан {e.args[0]}')
            return


class GetRoundMultiHandler(BaseHandler):
    async def get(self):
        try:
            game_id = int(self.get_argument('game'))
            await self.get_core_data('get_round', 'get', {'game': game_id})
        except (MissingArgumentError, ValueError):
            self.error(u'Не указана игра')

    async def post(self):
        data = self.data
        try:
            pars = {
                'game': data['game'],
                'theme': data['theme']
            }
            await self.get_core_data('get_round', 'post', pars)
        except KeyError as e:
            self.error(f'Не указан {e.args[0]}')
            return


class RoundMultiAnserHandler(BaseHandler):
    async def post(self):
        data = self.data
        try:
            pars = {
                'game': data['game'],
                'answers': data['answers']
            }
            await self.get_core_data('answer_round', 'post', pars)
        except KeyError as e:
            self.error(f'Не указан {e.args[0]}')
            return


class AddQuestionHandler(BaseHandler):
    async def post(self):
        data = self.data
        try:
            pars = {
                'question': data['question'],
                'answer_1': data['answer_1'],
                'answer_2': data['answer_2'],
                'answer_3': data['answer_3'],
                'answer_4': data['answer_4'],
                'correct': data['correct']
            }
            await self.get_core_data('add_question', 'post', pars)
        except KeyError as e:
            self.error(f'Не указан {e.args[0]}')
            return
