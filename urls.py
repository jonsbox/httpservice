from tornado.web import StaticFileHandler
import handlers


urls = (

    (r'/register', handlers.RegisterHandler),
    (r'/login', handlers.LoginHandler),

    (r'/start_app', handlers.StartAppHandler),
    (r'/new_game', handlers.NewGameHandler),
    (r'/get_question', handlers.GetQuestionHandler),
    (r'/answer', handlers.AnswerHandler),

    (r'/get_round', handlers.GetRoundMultiHandler),
    (r'/answer_round', handlers.RoundMultiAnserHandler),

    (r'/add_question', handlers.AddQuestionHandler),

    # (r'/media/(.*)', StaticFileHandler, {'path': '/home/jut/PycharmProjects/adminservice/adminservice/media'}),
    (r'/media/(.*)', StaticFileHandler, {'path': '/var/www/adminservice/adminservice/media'}),
)
